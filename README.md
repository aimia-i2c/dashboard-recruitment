# i2C

As part of the recruitment process we require the following task to be completed.

# Task
- Create a simple dashboard to present the data within a structure that will be legible and easily digested by end-users.
- This can include as many charts as you feel is required, plus consideration towards chart type.
- It may also include text describing the charts but please do not analyse the data, simply use Lorem Ipsum as representation.
- Style the dashboard as you see fit. However, please bear in mind that we utilise [MaterialUI] in one of our projects.
- We would like to see the front end fetching the data via an API.

### Tech
While we want you to implement your own ideas and creativity to this task we do have a few restrictions due to our current stack:

- [ReactJS]
- [node.js]
- [express]
- [Mariadb] - MySql is fine also

So, please no Angular, NoSQL, etc. And definitely no jQuery for the ui/ux

### Data
You will find the following files:
```
+-- data\
|   +-- weekly.csv - difference between exposed/control results per week per product.
|   +-- top.csv - top line values for each metric, value uplift and % uplift
```

Typically, weekly data is presented using a line chart with the top-line values being displayed in either a bar chart or table format.

### Questions
Should you have any questions, please feel free to reach out via email.

[node.js]: <http://nodejs.org>
[express]: <http://expressjs.com>
[ReactJS]: <https://reactjs.org/>
[Mariadb]: <https://mariadb.org/>
[MaterialUI]: <https://www.material-ui.com/#/>

